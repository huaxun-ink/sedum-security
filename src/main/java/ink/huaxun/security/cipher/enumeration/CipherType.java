package ink.huaxun.security.cipher.enumeration;

import ink.huaxun.security.cipher.constant.Algorithm;
import ink.huaxun.security.cipher.constant.Mode;
import ink.huaxun.security.cipher.constant.Padding;

/**
 * 加密类型枚举
 *
 * @author zhaogang
 * @date 2020-06-29 16:50
 */
public enum CipherType {

    RSA_PKCS1(Algorithm.RSA, Mode.ECB, Padding.PKCS1_PADDING),

    RSA_SHA1(Algorithm.RSA, Mode.ECB, Padding.SHA1_MGF1_PADDING),

    RSA_SHA256(Algorithm.RSA, Mode.ECB, Padding.SHA256_MGF1_PADDING),

    AES_ECB_PKCS5(Algorithm.AES, Mode.ECB, Padding.PKCS5_PADDING),

    AES_ECB_ISO10126(Algorithm.AES, Mode.ECB, Padding.ISO10126_PADDING),

    AES_CBC_PKCS5(Algorithm.AES, Mode.CBC, Padding.PKCS5_PADDING),

    AES_CBC_ISO10126(Algorithm.AES, Mode.CBC, Padding.ISO10126_PADDING),

    AES_CFB_PKCS5(Algorithm.AES, Mode.CFB, Padding.PKCS5_PADDING),

    AES_CFB_ISO10126(Algorithm.AES, Mode.CFB, Padding.ISO10126_PADDING),

    AES_OFB_PKCS5(Algorithm.AES, Mode.OFB, Padding.PKCS5_PADDING),

    AES_OFB_ISO10126(Algorithm.AES, Mode.OFB, Padding.ISO10126_PADDING),

    AES_PCBC_PKCS5(Algorithm.AES, Mode.PCBC, Padding.PKCS5_PADDING),

    AES_PCBC_ISO10126(Algorithm.AES, Mode.PCBC, Padding.ISO10126_PADDING),

    // 未加密
    NONE(null, null, null);

    private final String algorithm;

    private final String mode;

    private final String padding;

    CipherType(String algorithm, String mode, String padding) {
        this.algorithm = algorithm;
        this.mode = mode;
        this.padding = padding;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public String getCipher() {
        return algorithm + "/" + mode + "/" + padding;
    }

    public boolean getNeedIV() {
        return !Mode.ECB.equals(mode);
    }

}
