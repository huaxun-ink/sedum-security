package ink.huaxun.security.cipher;

/**
 * @author zhaogang
 * @description 加密算法接口
 * @date 2021-11-11 18:22
 */
public interface Cipher {

    String encrypt(String text);

    String decrypt(String text);

    byte[] encryptByte(String text);

    byte[] decryptByte(String text);

}
