package ink.huaxun.security.cipher;

import ink.huaxun.security.cipher.aes.AESFacade;
import ink.huaxun.security.cipher.enumeration.CipherType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

/**
 * AES操作
 *
 * @author zhaogang
 * @date 2020-06-29 16:51
 */
public class AES implements Cipher {

    private static final String SEED = "%e5%86%af%7e%7e+";

    private static final String IV = "%e5%b2%91%7e%7e+";

    private final CipherType cipherType;

    private final String seed;

    private final String iv;

    private AES(CipherType cipherType, String seed, String iv) {
        this.cipherType = cipherType;
        this.seed = seed;
        this.iv = iv;
    }

    public static AES getInstance(@NonNull CipherType cipherType) {
        return getInstance(cipherType, null);
    }

    public static AES getInstance(@NonNull CipherType cipherType, String seed) {
        return getInstance(cipherType, seed, null);
    }

    public static AES getInstance(@NonNull CipherType cipherType, String seed, String iv) {
        if (StringUtils.isBlank(seed)) {
            seed = SEED;
        }
        if (StringUtils.isBlank(iv)) {
            iv = IV;
        }
        return new AES(cipherType, seed, iv);
    }

    /**
     * 加密
     */
    @Override
    public String encrypt(String text) {
        return Base64.encodeBase64String(this.encryptByte(text));
    }

    /**
     * 解密
     */
    @Override
    public String decrypt(String text) {
        return new String(this.decryptByte(text));
    }

    /**
     * 加密
     */
    @Override
    public byte[] encryptByte(String text) {
        return AESFacade.encrypt(cipherType, text, seed, iv);
    }

    /**
     * 解密
     */
    @Override
    public byte[] decryptByte(String text) {
        return AESFacade.decrypt(cipherType, text, seed, iv);
    }

}
