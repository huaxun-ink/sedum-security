package ink.huaxun.security.cipher.aes;

import ink.huaxun.security.cipher.CipherFacade;
import ink.huaxun.security.cipher.enumeration.CipherType;
import ink.huaxun.security.spec.SecretKeyFacade;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.spec.AlgorithmParameterSpec;

/**
 * AES操作
 *
 * @author zhaogang
 * @date 2020-06-29 16:45
 */
public class AESFacade {

    /**
     * 加密
     */
    public static byte[] encrypt(CipherType cipherType, String text, String seed, String iv) {
        if (StringUtils.isBlank(text)) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }

        byte[] textByte = text.getBytes(StandardCharsets.UTF_8);
        SecretKey secretKey = SecretKeyFacade.getSecretKey(cipherType.getAlgorithm(), seed);
        AlgorithmParameterSpec vector = SecretKeyFacade.getVector(cipherType.getNeedIV(), iv);

        return CipherFacade.doFinal(textByte, cipherType, Cipher.ENCRYPT_MODE, secretKey, vector);
    }

    /**
     * 解密
     */
    public static byte[] decrypt(CipherType cipherType, String text, String seed, String iv) {
        if (StringUtils.isBlank(text)) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }

        byte[] textByte = Base64.decodeBase64(text);
        SecretKey secretKey = SecretKeyFacade.getSecretKey(cipherType.getAlgorithm(), seed);
        AlgorithmParameterSpec vector = SecretKeyFacade.getVector(cipherType.getNeedIV(), iv);

        return CipherFacade.doFinal(textByte, cipherType, Cipher.DECRYPT_MODE, secretKey, vector);
    }

}