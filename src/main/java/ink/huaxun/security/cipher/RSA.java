package ink.huaxun.security.cipher;

import ink.huaxun.security.cipher.enumeration.CipherType;
import ink.huaxun.security.cipher.rsa.RSAFacade;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * RSA操作
 *
 * @author zhaogang
 * @date 2020-06-29 16:51
 */
public class RSA implements Cipher, Asymmetric {

    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgA_zPesAN0iLvnhXm16l_O8GhJnRrWw-jDtJCXRllrN9uaSauTW8kaNY081DdJCrpK-bQEU3Z9IFlTHoazeQxzrEM3trcvNKNZEEYqLFfj2eBdL72U_ctBOnHmyd8H99Ot4aBLFnIeFnOzqIYAgJ5Qrp_CUfdL1BEACD5DMX-zQIDAQAB";

    private static final String PRIVATE_KEY = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKAD_M96wA3SIu-eFebXqX87waEmdGtbD6MO0kJdGWWs325pJq5NbyRo1jTzUN0kKukr5tARTdn0gWVMehrN5DHOsQze2ty80o1kQRiosV-PZ4F0vvZT9y0E6cebJ3wf3063hoEsWch4Wc7OohgCAnlCun8JR90vUEQAIPkMxf7NAgMBAAECgYAO6dA6FhqoLxmKc5Ve-mKVtSAKwObAyq_eZpc6ECs5yq7YP3yXaigdoJe2P6cdtS06phjQ84vp8FvO8R3AOcg2pmNznltZ0Eqes3XoAD1oKRAq3L7Py1w9ILynev6iKwTF6zIZRllLHoY1EEZYY8l2jLACkhnRNxmJ8GF3OK3ekQJBANR0kHk4WRRHoy03lI8EFG_t7hr4pphN3KDLu0rT-Fhf5l_zP7-50oAvf2p8GAcEMXXjiPTD_n3sbLQ9ZGpjmVsCQQDAz-7TUgkfOE5hwyBCW0vbkDYZfrPlZ5M9_IJadtcIFkCcT4FyjXU4_WXAjDzqUYELxLf2J4Yixd9DXSSODZj3AkANqSkZAkyIB0swvyAoSgTeZZgn19EufeMv26nBV_viAtiXVtbEhzhH0cYJnM0WtmhPwj-uLWQRyL_8nu30AU99AkBorvxd44ZsWufDkGttW6Ad_3g_81i8zrVUzEoBz9AD5gECXPBNP5xLatjZ58oS9ll4CEBQ9ziOnJwROPrl3WAVAkBi8BInNFsq7EO3aZEkbc2_Aa-rgQ8bY8JHbM4KE4Z-PyH-bmtfs6sqJ109TD7k5K9JehJHsWwdUwl_-3TE-oLU";

    private final CipherType cipherType;

    private final String publicKey;

    private final String privateKey;

    private RSA(CipherType cipherType, String publicKey, String privateKey) {
        this.cipherType = cipherType;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public static RSA getInstance(@NonNull CipherType cipherType) {
        return getInstance(cipherType, null, null);
    }

    public static RSA getInstance(@NonNull CipherType cipherType, String publicKey, String privateKey) {
        if (StringUtils.isBlank(publicKey)) {
            publicKey = PUBLIC_KEY;
        }
        if (StringUtils.isBlank(privateKey)) {
            privateKey = PRIVATE_KEY;
        }
        return new RSA(cipherType, publicKey, privateKey);
    }

    /**
     * 公钥加密
     */
    @Override
    public String encrypt(String text) {
        return Base64.encodeBase64String(this.encryptByte(text));
    }

    /**
     * 私钥解密
     */
    @Override
    public String decrypt(String text) {
        return new String(this.decryptByte(text));
    }

    /**
     * 公钥加密
     */
    @Override
    public byte[] encryptByte(String text) {
        return RSAFacade.encrypt(cipherType, text, getPublicKey());
    }

    /**
     * 私钥解密
     */
    @Override
    public byte[] decryptByte(String text) {
        return RSAFacade.decrypt(cipherType, text, getPrivateKey());
    }

    /**
     * 私钥签名
     */
    @Override
    public String signature(String text) {
        return Base64.encodeBase64String(this.signatureByte(text));
    }

    /**
     * 公钥验签
     */
    @Override
    public String verify(String text) {
        return new String(this.verifyByte(text));
    }

    /**
     * 私钥签名
     */
    @Override
    public byte[] signatureByte(String text) {
        return RSAFacade.signature(cipherType, text, getPrivateKey());
    }

    /**
     * 公钥验签
     */
    @Override
    public byte[] verifyByte(String text) {
        return RSAFacade.verify(cipherType, text, getPublicKey());
    }

    /**
     * 获取公钥
     */
    private PublicKey getPublicKey() {
        return RSAFacade.getPublicKey(cipherType, publicKey);
    }

    /**
     * 获取私钥
     */
    private PrivateKey getPrivateKey() {
        return RSAFacade.getPrivateKey(cipherType, privateKey);
    }

}
