package ink.huaxun.security.cipher.util;

import ink.huaxun.security.cipher.AES;
import ink.huaxun.security.cipher.enumeration.CipherType;

/**
 * 默认AES操作(AES/ECB/PKCS5Padding)
 *
 * @author zhaogang
 * @date 2020-06-29 16:51
 */
public class AESUtil {

    /**
     * 加密
     */
    public static String encrypt(String text) {
        return AES.getInstance(CipherType.AES_ECB_PKCS5).encrypt(text);
    }

    /**
     * 解密
     */
    public static String decrypt(String text) {
        return AES.getInstance(CipherType.AES_ECB_PKCS5).decrypt(text);
    }

}
