package ink.huaxun.security.cipher.util;

import ink.huaxun.security.cipher.RSA;
import ink.huaxun.security.cipher.enumeration.CipherType;

/**
 * 默认RSA操作(RSA/ECB/PKCS1Padding)
 *
 * @author zhaogang
 * @date 2020-06-29 16:51
 */
public class RSAUtil {

    /**
     * 公钥加密
     */
    public static String encrypt(String text) {
        return RSA.getInstance(CipherType.RSA_PKCS1).encrypt(text);
    }

    /**
     * 私钥解密
     */
    public static String decrypt(String text) {
        return RSA.getInstance(CipherType.RSA_PKCS1).decrypt(text);
    }

    /**
     * 私钥签名
     */
    public static String signature(String text) {
        return RSA.getInstance(CipherType.RSA_PKCS1).signature(text);
    }

    /**
     * 公钥验签
     */
    public static String verify(String text) {
        return RSA.getInstance(CipherType.RSA_PKCS1).verify(text);
    }

}
