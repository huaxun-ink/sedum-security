package ink.huaxun.security.cipher;

import ink.huaxun.security.cipher.enumeration.CipherType;
import org.apache.commons.lang3.ArrayUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

/**
 * 加密操作
 *
 * @author zhaogang
 * @date 2020-06-29 16:54
 */
public class CipherFacade {

    private static Cipher getInstance(CipherType cipherType) {
        try {
            return Cipher.getInstance(cipherType.getCipher());
        } catch (NoSuchPaddingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Cipher init(CipherType cipherType, int mode, Key key, AlgorithmParameterSpec iv) {
        Cipher cipher = getInstance(cipherType);
        if (cipher == null) {
            return null;
        }
        try {
            cipher.init(mode, key, iv);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return null;
        }
        return cipher;
    }

    public static byte[] doFinal(byte[] cipherByte, CipherType cipherType, int mode, Key key) {
        return doFinal(cipherByte, cipherType, mode, key, null);
    }

    public static byte[] doFinal(byte[] cipherByte, CipherType cipherType, int mode, Key key, AlgorithmParameterSpec iv) {
        Cipher cipher = init(cipherType, mode, key, iv);
        if (cipher == null) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }
        try {
            return cipher.doFinal(cipherByte);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }
    }

}
