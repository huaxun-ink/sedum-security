package ink.huaxun.security.cipher;

/**
 * @author zhaogang
 * @description 非对称加密算法接口
 * @date 2021-11-11 18:23
 */
public interface Asymmetric {

    String signature(String text);

    String verify(String text);

    byte[] signatureByte(String text);

    byte[] verifyByte(String text);

}
