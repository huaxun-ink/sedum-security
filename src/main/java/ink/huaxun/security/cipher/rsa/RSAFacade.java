package ink.huaxun.security.cipher.rsa;

import ink.huaxun.security.cipher.CipherFacade;
import ink.huaxun.security.cipher.enumeration.CipherType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * RSA操作
 *
 * @author zhaogang
 * @date 2020-06-29 16:48
 */
public class RSAFacade {

    /**
     * 获取公钥
     */
    public static PublicKey getPublicKey(CipherType cipherType, String publicKey) {
        KeyFactory keyFactory = KeyFacade.getInstance(cipherType);
        if (keyFactory == null) {
            return null;
        }
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey));
        try {
            return keyFactory.generatePublic(x509KeySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取私钥
     */
    public static PrivateKey getPrivateKey(CipherType cipherType, String privateKey) {
        KeyFactory keyFactory = KeyFacade.getInstance(cipherType);
        if (keyFactory == null) {
            return null;
        }
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
        try {
            return keyFactory.generatePrivate(pkcs8KeySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 公钥加密
     */
    public static byte[] encrypt(CipherType cipherType, String text, PublicKey publicKey) {
        if (StringUtils.isBlank(text)) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }
        return CipherFacade.doFinal(text.getBytes(StandardCharsets.UTF_8), cipherType, Cipher.ENCRYPT_MODE, publicKey);
    }

    /**
     * 私钥解密
     */
    public static byte[] decrypt(CipherType cipherType, String text, PrivateKey privateKey) {
        if (StringUtils.isBlank(text)) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }
        return CipherFacade.doFinal(Base64.decodeBase64(text), cipherType, Cipher.DECRYPT_MODE, privateKey);
    }

    /**
     * 私钥签名
     */
    public static byte[] signature(CipherType cipherType, String text, PrivateKey privateKey) {
        if (StringUtils.isBlank(text)) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }
        return CipherFacade.doFinal(text.getBytes(StandardCharsets.UTF_8), cipherType, Cipher.ENCRYPT_MODE, privateKey);
    }

    /**
     * 公钥验签
     */
    public static byte[] verify(CipherType cipherType, String text, PublicKey publicKey) {
        if (StringUtils.isBlank(text)) {
            return ArrayUtils.EMPTY_BYTE_ARRAY;
        }
        return CipherFacade.doFinal(Base64.decodeBase64(text), cipherType, Cipher.DECRYPT_MODE, publicKey);
    }

}
