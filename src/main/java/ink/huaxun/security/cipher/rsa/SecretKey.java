package ink.huaxun.security.cipher.rsa;

import org.apache.commons.codec.binary.Base64;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * RSA生成密钥
 *
 * @author zhaogang
 * @date 2020-06-29 16:48
 */
public class SecretKey {

    private static final int KEY_SIZE = 1024;

    private static final String ALGORITHM = "RSA";

    private static void createKeyPair() {
        createKeyPair(KEY_SIZE, ALGORITHM);
    }

    private static void createKeyPair(int keySize, String algorithm) {
        KeyPairGenerator keyPairGenerator;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            return;
        }
        keyPairGenerator.initialize(keySize);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        Key publicKey = keyPair.getPublic();
        System.out.println("publicKey is : " + Base64.encodeBase64URLSafeString(publicKey.getEncoded()));
        Key privateKey = keyPair.getPrivate();
        System.out.println("privateKey is : " + Base64.encodeBase64URLSafeString(privateKey.getEncoded()));
    }

    public static void main(String[] args) {
        createKeyPair(1024, "RSA");
    }

}
