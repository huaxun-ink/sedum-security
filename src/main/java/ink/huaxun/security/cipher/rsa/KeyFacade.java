package ink.huaxun.security.cipher.rsa;

import ink.huaxun.security.cipher.enumeration.CipherType;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;

/**
 * 创建密钥
 *
 * @author zhaogang
 * @date 2020-06-29 16:46
 */
public class KeyFacade {

    protected static KeyFactory getInstance(CipherType cipherType) {
        try {
            return KeyFactory.getInstance(cipherType.getAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

}
