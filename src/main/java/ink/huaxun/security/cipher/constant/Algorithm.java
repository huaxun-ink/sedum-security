package ink.huaxun.security.cipher.constant;

/**
 * 加密算法
 *
 * @author zhaogang
 * @date 2020-06-29 16:49
 */
public class Algorithm {

    public static final String RSA = "RSA";

    public static final String AES = "AES";

}
