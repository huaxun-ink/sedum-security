package ink.huaxun.security.cipher.constant;

/**
 * 补码方式
 *
 * @author zhaogang
 * @date 2020-06-29 16:50
 */
public class Padding {

    public static final String PKCS1_PADDING = "PKCS1Padding";

    public static final String PKCS5_PADDING = "PKCS5Padding";

    public static final String ISO10126_PADDING = "ISO10126Padding";

    public static final String SHA1_MGF1_PADDING = "OAEPWithSHA-1AndMGF1Padding";

    public static final String SHA256_MGF1_PADDING = "OAEPWithSHA-256AndMGF1Padding";

}
