package ink.huaxun.security.cipher.constant;

/**
 * 加密模式
 *
 * @author zhaogang
 * @date 2020-06-29 16:50
 */
public class Mode {

    public static final String CBC = "CBC";

    public static final String ECB = "ECB";

    public static final String CFB = "CFB";

    public static final String PCBC = "PCBC";

    public static final String OFB = "OFB";

}
