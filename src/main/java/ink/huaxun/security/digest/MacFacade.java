package ink.huaxun.security.digest;

import ink.huaxun.security.spec.SecretKeyFacade;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

public class MacFacade {

    protected static Mac getInstance(String algorithm) {
        try {
            return Mac.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Mac init(String algorithm, Key key, AlgorithmParameterSpec iv) {
        Mac mac = getInstance(algorithm);
        if (mac == null) {
            return null;
        }
        try {
            mac.init(key, iv);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return null;
        }
        return mac;
    }

    private static byte[] doFinal(byte[] digestByte, String algorithm, Key key) {
        return doFinal(digestByte, algorithm, key, null);
    }

    private static byte[] doFinal(byte[] digestByte, String algorithm, Key key, AlgorithmParameterSpec iv) {
        Mac mac = init(algorithm, key, iv);
        if (mac == null) {
            return new byte[0];
        }
        return mac.doFinal(digestByte);
    }

    /**
     * 暂时不实现iv相关
     */
    protected static String digest(String algorithm, String text, String seed) {
        if (StringUtils.isBlank(text)) {
            return StringUtils.EMPTY;
        }

        byte[] digestByte = text.getBytes(StandardCharsets.UTF_8);
        SecretKey secretKey = SecretKeyFacade.getSecretKey(algorithm, seed);

        return Hex.encodeHexString(doFinal(digestByte, algorithm, secretKey));
    }

}
