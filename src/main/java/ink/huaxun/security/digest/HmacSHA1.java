package ink.huaxun.security.digest;

import org.apache.commons.lang3.StringUtils;

/**
 * Hmac-SHA1操作
 *
 * @author zhaogang
 * @date 2022-05-12 17:51
 */
public class HmacSHA1 {

    private static final String ALGORITHM = "HmacSHA1";

    private static final String SEED = "%e5%86%af%7e%7e+";

    private final String seed;

    private HmacSHA1(String seed) {
        this.seed = seed;
    }

    public static HmacSHA1 getInstance() {
        return getInstance(null);
    }

    public static HmacSHA1 getInstance(String seed) {
        if (StringUtils.isBlank(seed)) {
            seed = SEED;
        }
        return new HmacSHA1(seed);
    }

    public String digest(String text) {
        return MacFacade.digest(ALGORITHM, text, seed);
    }

}
