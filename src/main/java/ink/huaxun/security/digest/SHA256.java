package ink.huaxun.security.digest;

import java.security.MessageDigest;

public class SHA256 {

    private static final String ALGORITHM = "SHA-256";

    private static final String SALT = "salt";

    public static MessageDigest getInstance() {
        return DigestFacade.getInstance(ALGORITHM);
    }

    public static String digest(String text) {
        return DigestFacade.digest(ALGORITHM, text);
    }

    public static String saltDigest(String text) {
        return saltDigest(text, SALT);
    }

    public static String saltDigest(String text, String salt) {
        return digest(digest(text) + salt);
    }

}
