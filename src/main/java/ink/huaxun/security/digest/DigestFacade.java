package ink.huaxun.security.digest;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestFacade {

    protected static MessageDigest getInstance(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static String digest(String algorithm, String text) {
        if (StringUtils.isBlank(text)) {
            return StringUtils.EMPTY;
        }
        MessageDigest messageDigest = getInstance(algorithm);
        if (messageDigest == null) {
            return StringUtils.EMPTY;
        }
        byte[] digestByte = messageDigest.digest(text.getBytes(StandardCharsets.UTF_8));
        return Hex.encodeHexString(digestByte);
    }

}
