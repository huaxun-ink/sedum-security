package ink.huaxun.security.spec;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.spec.AlgorithmParameterSpec;

/**
 * 密钥操作
 *
 * @author zhaogang
 * @date 2022-05-12 17:35
 */
public class SecretKeyFacade {

    /**
     * 获取密钥
     */
    public static SecretKey getSecretKey(String algorithm, String seed) {
        return new SecretKeySpec(seed.getBytes(StandardCharsets.UTF_8), algorithm);
    }

    /**
     * 获取向量
     */
    public static AlgorithmParameterSpec getVector(boolean needIV, String iv) {
        if (!needIV) {
            return null;
        }
        return new IvParameterSpec(iv.getBytes(StandardCharsets.UTF_8));
    }

}
